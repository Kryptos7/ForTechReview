using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using System.Threading.Tasks;
using System.Diagnostics; // Stopwatch를 사용하기 위해 추가

public class AssetLoader : MonoBehaviour
{
    [field: SerializeField]
    public LoaderModule LoaderModule { get; set; }
    private void Start()
    {
        // Application.dataPath를 사용하여 프로젝트의 Assets 폴더 경로를 얻고, 지정된 하위 디렉토리에서 .obj 파일을 검색
        List<string> selectedAssetNames = GetObjFiles(Application.dataPath + "/Resources/Models");
        Load(selectedAssetNames);
    }


    private List<string> GetObjFiles(string directory)
    {
        // 지정된 디렉토리에서 .obj 파일들의 경로를 찾아 리스트로 반환
        var fullPath = Path.Combine(Application.dataPath, directory);
        var objFiles = Directory.GetFiles(fullPath, "*.obj", SearchOption.AllDirectories).ToList();

        // 파일 크기에 따라 정렬하기 위해 파일 정보를 불러온 후 크기와 경로를 함께 저장
        // 파일 크기에 따라 오름차순으로 정렬
        var objFilesWithSize = objFiles.Select(file => new FileInfo(file))
                                       .OrderBy(fileInfo => fileInfo.Length) // 오름차순으로 정렬
                                       .Select(fileInfo => fileInfo.FullName)
                                       .ToList();

        return objFilesWithSize;
    }


    public async void Load(List<string> assetPaths)
    {
        Stopwatch stopwatch = new Stopwatch(); // Stopwatch 인스턴스 생성
        stopwatch.Start(); // 시간 측정 시작

        // assetPaths 리스트에 있는 각 .obj 파일에 대해 비동기 로딩 수행
        foreach (var path in assetPaths)
        {
            // 비동기 로딩 작업을 실행하고 완료될 때까지 대기
            GameObject loadedAsset = await LoaderModule.LoadAssetAsync(path);

            if (loadedAsset != null)
            {
                // 로드된 객체를 현재 GameObject의 자식으로 설정
                loadedAsset.transform.SetParent(this.transform);
            }
        }

        stopwatch.Stop(); // 시간 측정 종료

        // 작업 완료 메시지와 함께 걸린 시간 출력
        UnityEngine.Debug.Log($"작업 완료. 걸린 시간: {stopwatch.Elapsed.TotalSeconds:00.00}초");

    }
}
