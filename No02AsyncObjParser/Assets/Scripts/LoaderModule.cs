using System;
using System.Collections;
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using static ObjImporter;

public class ObjImporter
{
    // MeshData 클래스: 메시(3D 모델)의 데이터를 저장하는 클래스
    public class MeshData
    {
        public Vector3[] Vertices; // 정점 배열: 메시를 구성하는 각 정점의 위치를 저장
        public int[] Triangles; // 삼각형 배열: 메시를 구성하는 삼각형의 정점 인덱스를 저장
        public Vector2[] UVs; // UV 배열: 메시의 각 정점에 대한 텍스처 좌표를 저장
        public Vector3[] Normals; // 노멀 배열: 메시의 각 정점에 대한 노멀 벡터(표면의 방향)를 저장
    }

    // MaterialInfo 클래스: 메터리얼(재질) 정보를 저장하는 클래스
    public class MaterialInfo
    {
        public string Name; // 이름: 재질의 이름을 저장
        public Color Color = Color.white; // 색상: 재질의 색상을 저장. 기본값으로 흰색 설정
        public string DiffuseTexturePath; // 디퓨즈 텍스처 경로: 재질의 디퓨즈(기본) 텍스처 파일 경로를 저장
        public string BumpMapPath; // 범프 맵 경로: 재질의 범프 맵(표면의 높낮이를 표현하는 텍스처) 파일 경로를 저장
    }

    public static MeshData ImportFile(string filePath)
    {
        // 실제 마지막에 적용될 정점, 삼각형, UV 좌표를 저장할 리스트
        List<Vector3> newVertices = new List<Vector3>();
        List<int> newTriangles = new List<int>();
        List<Vector2> newUVs = new List<Vector2>();
        List<Vector3> newNormals = new List<Vector3>();

        // 정점 인덱스를 기반으로 하며, UV 인덱스와 노멀 인덱스가 있는 경우 이를 조합한 문자열을 키로 사용. 정점의 고유성을 보장하기 위한 매핑
        Dictionary<string, int> vertexUVIndexMap = new Dictionary<string, int>();

        // 파일에서 읽은 원본 정점과 UV 좌표, 노멀을 저장할 리스트
        List<Vector3> originalVertices = new List<Vector3>();
        List<Vector2> originalUVs = new List<Vector2>();
        List<Vector3> originalNormals = new List<Vector3>();

        // 파일을 읽어서 각 라인별로 처리
        string[] lines = File.ReadAllLines(filePath);
        foreach (var line in lines)
        {
            // 정점 데이터 라인
            if (line.StartsWith("v "))
            {
                // 정점 데이터를 파싱하여 Vector3 객체로 변환
                string[] vertexData = line.Substring(2).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                Vector3 vertex = new Vector3(float.Parse(vertexData[0], CultureInfo.InvariantCulture),
                                             float.Parse(vertexData[1], CultureInfo.InvariantCulture),
                                             float.Parse(vertexData[2], CultureInfo.InvariantCulture));
                // 파싱된 정점을 리스트에 추가
                originalVertices.Add(vertex);
            }

            // UV 데이터 라인
            else if (line.StartsWith("vt "))
            {
                // UV 데이터를 파싱하여 Vector2 객체로 변환
                string[] uvData = line.Substring(3).Split(' ');
                Vector2 uvCoord = new Vector2(float.Parse(uvData[0], CultureInfo.InvariantCulture),
                                              float.Parse(uvData[1], CultureInfo.InvariantCulture));
                // 파싱된 UV 좌표를 리스트에 추가
                originalUVs.Add(uvCoord);
            }
            // 파일을 읽고 각 라인별로 처리하는 부분에 다음 조건문을 추가
            else if (line.StartsWith("vn "))
            {
                // 노멀 데이터를 파싱하여 Vector3 객체로 변환
                string[] normalData = line.Substring(3).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                Vector3 normal = new Vector3(
                    float.Parse(normalData[0], CultureInfo.InvariantCulture),
                    float.Parse(normalData[1], CultureInfo.InvariantCulture),
                    float.Parse(normalData[2], CultureInfo.InvariantCulture));
                // 파싱된 노멀을 리스트에 추가
                originalNormals.Add(normal);
            }
            // 면(삼각형) 데이터 라인
            else if (line.StartsWith("f "))
            {
                // 면을 구성하는 각 정점을 처리
                string[] parts = line.Substring(2).Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                // 면을 구성하는 정점 데이터를 분석. 사각형은 두 개의 삼각형으로 분할됨.
                for (int i = 0; i < parts.Length - 2; i++)
                {
                    // 하나의 면을 구성하는 삼각형의 각 꼭지점에 대해 반복
                    for (int j = 0; j < 3; j++)
                    {
                        // 삼각형을 구성하는 각 정점을 선택. 첫 번째 정점은 항상 포함
                        int partIndex = j == 0 ? 0 : i + j;
                        string[] subParts = parts[partIndex].Split('/');


                        // 각 부분에서 정점 인덱스, UV 인덱스, 법선 인덱스를 파싱
                        int vertexIndex = int.Parse(subParts[0]) - 1;// OBJ 인덱스는 1부터 시작하므로 1을 빼준다. 아래 역시 마찬가지.
                        int uvIndex = -1, normalIndex = -1;
                        if (subParts.Length > 1 && !string.IsNullOrWhiteSpace(subParts[1]))
                        {
                            uvIndex = int.Parse(subParts[1]) - 1;
                        }
                        if (subParts.Length > 2)
                        {
                            normalIndex = int.Parse(subParts[2]) - 1;
                        }

                        // 정점과 UV 인덱스의 조합을 키로 사용하여 새로운 정점을 관리
                        string key = vertexIndex.ToString(); // 정점 인덱스만 사용한 기본 키
                        if (uvIndex != -1)
                        {
                            key += $"_{uvIndex}"; // UV 인덱스가 있는 경우 키에 추가. $는 ToString을 안쓰기 위한 문자열 보간
                        }


                        // 이 키에 대한 새로운 정점 인덱스를 찾거나 생성
                        if (!vertexUVIndexMap.TryGetValue(key, out int newIndex))
                        {
                            // 새로운 정점을 newVertices 리스트에 추가
                            newVertices.Add(originalVertices[vertexIndex]);
                            // 유효한 UV 인덱스가 있는 경우, 해당 UV를 newUVs 리스트에 추가
                            if (uvIndex != -1)
                            {
                                newUVs.Add(originalUVs[uvIndex]);
                            }
                            // 유효한 노멀 인덱스가 있고, 이 인덱스가 originalNormals 범위 내에 있는 경우, 해당 노멀을 newNormals 리스트에 추가
                            if (normalIndex != -1 && originalNormals.Count > normalIndex)
                            {
                                newNormals.Add(originalNormals[normalIndex]);
                            }

                            // 새로운 정점의 인덱스를 구하고, 이를 매핑
                            newIndex = newVertices.Count - 1;
                            // 현재 정점과 UV 인덱스 조합을 새 인덱스에 매핑
                            vertexUVIndexMap.Add(key, newIndex);
                        }

                        // 새로운 정점 인덱스를 삼각형의 인덱스 배열에 추가
                        newTriangles.Add(newIndex);
                    }
                }
            }


        }


        // MeshData 객체를 반환, 정점, 삼각형 인덱스, UVs, 노멀 배열 포함
        return new MeshData
        {
            Vertices = newVertices.ToArray(),
            Triangles = newTriangles.ToArray(),
            UVs = newUVs.ToArray(),
            Normals = newNormals.ToArray()
        };

    }


    // MTL 파일을 파싱하여 MaterialInfo 객체의 리스트로 변환하는 메서드
    public static List<MaterialInfo> ParseMtlFile(string mtlPath)
    {
        var materialsInfoList = new List<MaterialInfo>(); // 재질 정보를 저장할 리스트
        MaterialInfo currentMaterialInfo = null; // 현재 파싱 중인 재질 정보

        // MTL 파일을 읽기 위한 StreamReader 생성
        using (var reader = new StreamReader(mtlPath))
        {
            string line;
            // 파일의 끝에 도달할 때까지 한 줄씩 읽기
            while ((line = reader.ReadLine()) != null)
            {
                line = line.Trim(); // 공백 제거

                // 새로운 재질 선언이 시작되는 경우
                if (line.StartsWith("newmtl "))
                {
                    // 이전에 파싱 중이던 재질 정보가 있으면 리스트에 추가
                    if (currentMaterialInfo != null)
                    {
                        materialsInfoList.Add(currentMaterialInfo);
                    }
                    // 새 재질 정보 객체 생성 및 이름 설정
                    currentMaterialInfo = new MaterialInfo
                    {
                        Name = line.Substring(7).Trim()
                    };
                }

                // 현재 재질 정보가 유효한 경우 추가 파싱 진행
                if (currentMaterialInfo != null)
                {
                    // 재질의 색상(Kd) 파싱
                    if (line.StartsWith("Kd "))
                    {
                        string[] colorComponents = line.Substring(3).Trim().Split(' ');
                        if (colorComponents.Length == 3 &&
                            float.TryParse(colorComponents[0], NumberStyles.Any, CultureInfo.InvariantCulture, out float r) &&
                            float.TryParse(colorComponents[1], NumberStyles.Any, CultureInfo.InvariantCulture, out float g) &&
                            float.TryParse(colorComponents[2], NumberStyles.Any, CultureInfo.InvariantCulture, out float b))
                        {
                            currentMaterialInfo.Color = new Color(r, g, b);
                        }
                    }
                    // 디퓨즈 텍스처 경로(map_Kd) 파싱
                    else if (line.StartsWith("map_Kd "))
                    {
                        currentMaterialInfo.DiffuseTexturePath = Path.Combine(Path.GetDirectoryName(mtlPath), line.Substring(7).Trim());
                    }
                    // 범프 맵 경로(map_bump 또는 bump) 파싱
                    else if (line.StartsWith("map_bump ") || line.StartsWith("bump "))
                    {
                        currentMaterialInfo.BumpMapPath = Path.Combine(Path.GetDirectoryName(mtlPath), line.Substring(line.IndexOf(' ') + 1).Trim());
                    }
                }
            }

            // 마지막으로 파싱 중이던 재질 정보가 있다면 리스트에 추가
            if (currentMaterialInfo != null)
            {
                materialsInfoList.Add(currentMaterialInfo);
            }
        }

        // 파싱된 재질 정보 리스트 반환
        return materialsInfoList;
    }


    // 텍스처를 로드 함수
    public static Texture2D LoadTexture(string texturePath)
    {
        if (File.Exists(texturePath))
        {
            var textureBytes = File.ReadAllBytes(texturePath);
            var texture = new Texture2D(2, 2);
            if (texture.LoadImage(textureBytes))
            {
                return texture;
            }
        }
        return null;
    }
}
public class LoaderModule : MonoBehaviour
{
    //GameObject 로드 완료 시 호출
    public Action<GameObject> OnLoadCompleted;

    public async Task<GameObject> LoadAssetAsync(string filePath)
    {
        if (!File.Exists(filePath))
        {
            Debug.LogError("파일 없음: " + filePath);
            return null;
        }

        //meshData를 로딩. 객체를 만드는 것은 메인스레드에서만 가능함
        MeshData meshData = await Task.Run(() => ObjImporter.ImportFile(filePath));

        if (meshData == null)
        {
            Debug.LogError(".obj 파싱 실패");
            return null;
        }

        GameObject loadedObject = null;

        // MainThreadDispatcher를 사용하여 메인 스레드에서 객체 생성
        await MainThreadDispatcher.ExecuteOnMainThreadAsync(() =>
        {
            //파일 이름으로 객체 생성
            loadedObject = new GameObject(Path.GetFileNameWithoutExtension(filePath));

            //생성된 객체에 Render용 컴포넌트들 추가
            var meshFilter = loadedObject.AddComponent<MeshFilter>();
            var meshRenderer = loadedObject.AddComponent<MeshRenderer>();

            // Mesh 객체 생성 및 MeshData 데이터 적용
            var mesh = new Mesh
            {
                vertices = meshData.Vertices,// 정점 위치 설정
                triangles = meshData.Triangles,// 메시의 삼각형 인덱스 설정
                uv = meshData.UVs,// 텍스처 좌표 설정
                normals = meshData.Normals // 정점의 법선 설정
            };

            meshFilter.mesh = mesh; // 생성된 Mesh를 MeshFilter에 할당
        });

        string mtlPath = Path.ChangeExtension(filePath, ".mtl");
        // .mtl 파일 파싱을 통해 MaterialInfo 목록 로드. 마찬가지로 생성은 오로지 메인스레드에서만 가능.
        List<MaterialInfo> materialInfos = await Task.Run(() => ObjImporter.ParseMtlFile(mtlPath));
        List<Material> materialsList = new List<Material>();

        // 메인 스레드에서 Material 객체 생성 및 적용
        await MainThreadDispatcher.ExecuteOnMainThreadAsync(() =>
        {
            foreach (var materialInfo in materialInfos)
            {
                var material = new Material(Shader.Find("Standard"));
                // materialInfo에서 정보를 가져와 material 설정 (color, mainTexture 등)
                if (!string.IsNullOrEmpty(materialInfo.DiffuseTexturePath))
                {
                    // 메인 스레드 작업이 예약된 후 순차적으로 실행된다.
                    Texture2D texture = LoadTexture(materialInfo.DiffuseTexturePath);
                    material.mainTexture = texture;
                }

                // 범프 맵 적용.(있다면!!)
                if (!string.IsNullOrEmpty(materialInfo.BumpMapPath))
                {
                    Texture2D bumpMap = LoadTexture(materialInfo.BumpMapPath);
                    if (bumpMap != null)
                    {
                        // Unity의 Standard 셰이더는 _BumpMap 프로퍼티를 사용하여 범프 맵을 적용
                        material.SetTexture("_BumpMap", bumpMap);
                        material.EnableKeyword("_NORMALMAP");

                    }
                }

                // 처리된 Material을 목록에 추가
                materialsList.Add(material);
            }

            // 생성된 materialsList를 MeshRenderer에 적용
            if (loadedObject != null)
            {
                loadedObject.GetComponent<MeshRenderer>().materials = materialsList.ToArray();
            }
        });

        OnLoadCompleted?.Invoke(loadedObject);
        return loadedObject;
    }
}
