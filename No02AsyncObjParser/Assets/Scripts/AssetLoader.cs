using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AssetLoader : MonoBehaviour
{
    [field: SerializeField]
    public LoaderModule LoaderModule { get; set; }
    string modelPath = Application.dataPath + "/Model/Cat/12221_Cat_v1_l3.obj";


    private void Start()
    {
        Load(modelPath);
    }


    public async void Load(string assetName)
    {
        if (!string.IsNullOrEmpty(assetName))
        {
            GameObject loadedAsset = await LoaderModule.LoadAssetAsync(assetName);
            if (loadedAsset != null) // 로딩 성공 확인
            {
                loadedAsset.transform.SetParent(transform);
            }
        }
    }
}
