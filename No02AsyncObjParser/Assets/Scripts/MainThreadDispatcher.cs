using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Threading.Tasks;

/// <summary>
/// 메인스레드와 서브 스레드 간의 중재자 역할을 할 클래스.
/// 실행할 작업을 큐로 쌓고, 큐에 쌓은 내용을 Update에서 실행하는 구조
/// 객체를 새로 생성하는 종류의 일은 메인스레드에서만이 할수 있으므로 이런식으로 사용
/// </summary>
public class MainThreadDispatcher : MonoBehaviour
{

    //작업을 쌓을 큐
    private static readonly ConcurrentQueue<Action> queue = new ConcurrentQueue<Action>();

    private static MainThreadDispatcher instance;

    private void Awake()
    {
        // MainThreadDispatcher의 인스턴스 설정
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    /// <summary>
    /// 메인 스레드 작업 예약
    /// </summary>
    /// <param name="action">실행할 작업.</param>
    /// <returns>작업의 완료를 알려줄 Task 객체</returns>
    public static Task ExecuteOnMainThreadAsync(Action action)
    {
        if (action == null) throw new ArgumentNullException(nameof(action));

        var tcs = new TaskCompletionSource<bool>();

        // 메인 스레드에서 실행할 작업을 큐에 추가
        ExecuteOnMainThread(() =>
        {
            try
            {
                action();
                tcs.SetResult(true); // 작업이 성공적으로 완료되면, Task를 완료 상태로 설정
            }
            catch (Exception ex)
            {
                tcs.SetException(ex); // 예외 발생 시, Task에 예외를 설정
            }
        });

        return tcs.Task;
    }


    /// <summary>
    /// 작업 내용을 큐에 추가
    /// </summary>
    /// <param name="action">큐에 추가할 작업</param>
    static void ExecuteOnMainThread(Action action)
    {
        if (action == null) throw new ArgumentNullException(nameof(action));

        queue.Enqueue(action);
    }

    void Update()
    {
        // 큐에 있는 모든 작업을 실행
        while (queue.TryDequeue(out var action))
        {
            action.Invoke();
        }
    }
}