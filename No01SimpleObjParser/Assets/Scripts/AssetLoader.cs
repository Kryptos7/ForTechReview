using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetLoader : MonoBehaviour
{
    [SerializeField]
    private LoaderModule loaderModule;
    string modelPath = Application.dataPath + "/Model/Cat/12221_Cat_v1_l3.obj";
    private void Start()
    {
        string selectedAssetName = modelPath;
        Load(selectedAssetName);
    }

    public void Load(string assetName)
    {
        loaderModule.OnLoadCompleted += OnLoadCompleted; // 로드 완료 이벤트 구독
        loaderModule.LoadAsset(assetName);
    }

    private void OnLoadCompleted(GameObject loadedAsset)
    {
        loadedAsset.transform.SetParent(transform);
    }
}
